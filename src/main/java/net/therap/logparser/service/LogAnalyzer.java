package net.therap.logparser.service;

import net.therap.logparser.model.AnalysisModel;
import net.therap.logparser.model.LogModel;
import net.therap.logparser.util.DateTimeUtil;

import java.util.*;

/**
 * @author Shafin Mahmud
 * @since 11/2/16
 */
public class LogAnalyzer {

    public enum TimeSplit{
        TIME_SPLIT_BY_DAYS,
        TIME_SPLIT_BY_HOURS,
        TIME_SPLIT_BY_MINUTES,
    }

    private final TimeSplit TIME_INTERVAL;
    private final List<LogModel> LOGS;

    public LogAnalyzer(List<LogModel> LOGS, TimeSplit interval) {
        this.LOGS = LOGS;
        this.TIME_INTERVAL = interval;
    }

    public Map<Date, AnalysisModel> getTimeIntervaledAnalysis(){
        long maxTime = 0;
        long minTime = LOGS.size() > 0 ? LOGS.get(0).getTime().getTime() : 9999999999999L;

        for(LogModel log : LOGS){
            maxTime = log.getTime().getTime() > maxTime ? log.getTime().getTime() : maxTime;
            minTime = log.getTime().getTime() < minTime ? log.getTime().getTime() : minTime;
        }

        switch (TIME_INTERVAL){
            case TIME_SPLIT_BY_DAYS:
                break;
            case TIME_SPLIT_BY_HOURS:
                List<Date> hourIntervals = DateTimeUtil.getHoursBetween(new Date(minTime), new Date(maxTime));
                Map<Date, AnalysisModel> map = analyzeLogs(hourIntervals);
                return map;

            case TIME_SPLIT_BY_MINUTES:
                List<Date> minIntervals = DateTimeUtil.getMinutesBetween(new Date(minTime), new Date(maxTime));
                map = analyzeLogs(minIntervals);
                return map;

            default:
                hourIntervals = DateTimeUtil.getHoursBetween(new Date(minTime), new Date(maxTime));
                map = analyzeLogs(hourIntervals);
                return map;

        }

        return null;
    }

    private  Map<Date, AnalysisModel> analyzeLogs(List<Date> laps){
        Map<Date, AnalysisModel> map = new TreeMap<Date, AnalysisModel>();
        for(int i = 0; i < laps.size(); i++){
            Date begin = laps.get(i);
            Date end;

            switch (TIME_INTERVAL){
                case TIME_SPLIT_BY_HOURS:
                    end = DateTimeUtil.getDateAfterHour(begin, 1);
                    break;
                case TIME_SPLIT_BY_MINUTES:
                    end = DateTimeUtil.getDateAfterMinute(begin, 1);
                    break;
                default:
                    end = DateTimeUtil.getDateAfterHour(begin, 1);
                    break;
            }

            AnalysisModel analysisModel = new AnalysisModel();
            analysisModel.setRangeBegin(begin);
            analysisModel.setRangeEnd(end);

            map.put(begin, analysisModel);
        }

        for(LogModel log : LOGS){
            AnalysisModel analysis = map.get(generateTimeIntervalKey(log.getTime()));

            if(log.getRequestType().equals("G")){
                analysis.setRequestCountGet(analysis.getRequestCountGet() + 1);
            }else if(log.getRequestType().equals("P")){
               analysis.setRequestCountPost(analysis.getRequestCountPost() + 1);
            }

            analysis.getUniqueURIs().add(log.getUri());
            analysis.setTotalResponseTime(analysis.getTotalResponseTime()+log.getExecutionTime());
        }


        /*removing map key having unique URI count 0*/
        Iterator<Map.Entry<Date, AnalysisModel>> iter = map.entrySet().iterator();
        while (iter.hasNext()) {
            Map.Entry<Date, AnalysisModel> entry = iter.next();
            if(entry.getValue().getUniqueURIs().size() < 1){
                iter.remove();
            }
        }
        return map;
    }

    private Date generateTimeIntervalKey(Date logTime){
        Calendar cal = Calendar.getInstance();
        cal.setTime(logTime);

        switch (TIME_INTERVAL){
            case TIME_SPLIT_BY_HOURS:
                cal.set(Calendar.MINUTE, 0);
                cal.set(Calendar.SECOND, 0);
                cal.set(Calendar.MILLISECOND, 0);
                return cal.getTime();
            case TIME_SPLIT_BY_MINUTES:
                cal.set(Calendar.SECOND, 0);
                cal.set(Calendar.MILLISECOND, 0);
                return cal.getTime();
            default:
                cal.set(Calendar.MINUTE, 0);
                cal.set(Calendar.SECOND, 0);
                cal.set(Calendar.MILLISECOND, 0);
                return cal.getTime();
        }
    }
}
