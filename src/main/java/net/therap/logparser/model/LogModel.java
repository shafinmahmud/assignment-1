package net.therap.logparser.model;

import java.util.Date;

/**
 * @author Shafin Mahmud
 * @since 11/2/16
 */
public class LogModel {

    private Date time;
    private String uri;
    private String requestType;
    public long executionTime;

    public LogModel(){

    }

    public Date getTime() {
        return time;
    }

    public void setTime(Date time) {
        this.time = time;
    }

    public String getUri() {
        return uri;
    }

    public void setUri(String uri) {
        this.uri = uri;
    }

    public String getRequestType() {
        return requestType;
    }

    public void setRequestType(String requestType) {
        this.requestType = requestType;
    }

    public long getExecutionTime() {
        return executionTime;
    }

    public void setExecutionTime(long executionTime) {
        this.executionTime = executionTime;
    }

    @Override
    public String toString() {
        return "LogModel{" +
                "time=" + time +
                ", uri='" + uri + '\'' +
                ", requestType='" + requestType + '\'' +
                ", executionTime=" + executionTime +
                '}';
    }
}
