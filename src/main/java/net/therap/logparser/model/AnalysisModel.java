package net.therap.logparser.model;

import net.therap.logparser.util.DateTimeUtil;

import java.util.Date;
import java.util.HashSet;
import java.util.Set;

/**
 * @author Shafin Mahmud
 * @since 11/2/16
 */
public class AnalysisModel implements Comparable<AnalysisModel>{

    private Date rangeBegin;
    private Date rangeEnd;

    private long requestCountGet;
    private long requestCountPost;

    private Set<String> uniqueURIs;
    private long totalResponseTime;

    public AnalysisModel() {
        this.uniqueURIs = new HashSet<String>();
    }

    public Date getRangeBegin() {
        return rangeBegin;
    }

    public void setRangeBegin(Date rangeBegin) {
        this.rangeBegin = rangeBegin;
    }

    public Date getRangeEnd() {
        return rangeEnd;
    }

    public void setRangeEnd(Date rangeEnd) {
        this.rangeEnd = rangeEnd;
    }

    public long getRequestCountGet() {
        return requestCountGet;
    }

    public void setRequestCountGet(long requestCountGet) {
        this.requestCountGet = requestCountGet;
    }

    public long getRequestCountPost() {
        return requestCountPost;
    }

    public void setRequestCountPost(long requestCountPost) {
        this.requestCountPost = requestCountPost;
    }

    public Set<String> getUniqueURIs() {
        return uniqueURIs;
    }

    public void setUniqueURIs(Set<String> uniqueURIs) {
        this.uniqueURIs = uniqueURIs;
    }

    public long getTotalResponseTime() {
        return totalResponseTime;
    }

    public void setTotalResponseTime(long totalResponseTime) {
        this.totalResponseTime = totalResponseTime;
    }

    @Override
    public String toString() {
        String time = DateTimeUtil.formatToHoursOnly(rangeBegin)+" - "+DateTimeUtil.formatToHoursOnly(rangeEnd);
        String reqCount = requestCountGet+"/"+requestCountPost;
        return String.format("%20s%20s%30d%30sms",time,reqCount,uniqueURIs.size(), totalResponseTime);
    }

    @Override
    public int compareTo(AnalysisModel o) {
        long thisReq = this.getRequestCountGet()+this.getRequestCountPost();
        long oReq = o.getRequestCountGet()+o.getRequestCountPost();
        return (int)(oReq - thisReq);
    }
}
