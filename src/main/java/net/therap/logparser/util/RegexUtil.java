package net.therap.logparser.util;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * @author Shafin Mahmud
 * @since 11/2/16
 */
public class RegexUtil {

    public static String getFirstMatchedGroup(String text, String regex) {
        Pattern pattern = Pattern.compile(regex);
        Matcher matcher = pattern.matcher(text);
        if (matcher.find()) {
            return matcher.group(0);
        }
        return null;
    }

    public static String getSecondMatchedGroup(String text, String regex) {
        Pattern pattern = Pattern.compile(regex);
        Matcher matcher = pattern.matcher(text);
        if (matcher.find()) {
            return matcher.group(1);
        }
        return null;
    }


    public static void main(String[] args) {
        String regex = "URI=\\[(.*)\\]";
        String text = "2013-08-07 11:24:06,784 [[ACTIVE] ExecuteThread: '0' for queue: 'weblogic.kernel.Default "
                + "(self-tuning)'] U:osl@OSL-NM A:fbd0e570 [PROFILER:132] DEBUG - URI=[/ma/entry], G, time=9ms";

        System.out.println(getSecondMatchedGroup(text, regex));
    }

}
