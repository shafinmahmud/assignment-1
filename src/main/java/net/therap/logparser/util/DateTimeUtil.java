package net.therap.logparser.util;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

/**
 * @author Shafin Mahmud
 * @since 11/2/16
 */
public class DateTimeUtil {

    public static Date parseDateString(String dateString) {
        try {
            dateString = dateString.replace(",", ".");
            Date date = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSSSSS").parse(dateString);
            return date;
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return null;
    }


    public static List<Date> getHoursBetween(Date from, Date to) {
        List<Date> hours = new ArrayList<Date>();
        Calendar cal = Calendar.getInstance();
        cal.setTime(from);
        cal.set(Calendar.MINUTE, 0);
        cal.set(Calendar.SECOND, 0);
        cal.set(Calendar.MILLISECOND, 0);

        while (cal.getTime().before(to)) {
            hours.add(cal.getTime());
            cal.add(Calendar.HOUR, 1);
        }
        return hours;
    }

    public static List<Date> getMinutesBetween(Date from, Date to) {
        List<Date> minutes = new ArrayList<Date>();
        Calendar cal = Calendar.getInstance();
        cal.setTime(from);
        cal.set(Calendar.SECOND, 0);
        cal.set(Calendar.MILLISECOND, 0);

        while (cal.getTime().before(to)) {
            minutes.add(cal.getTime());
            cal.add(Calendar.MINUTE, 1);
        }
        return minutes;
    }

    public static Date getDateAfterHour(Date seedDate, int hour) {
        Calendar cal = Calendar.getInstance();
        cal.setTime(seedDate);
        cal.add(Calendar.HOUR, hour);
        return cal.getTime();
    }

    public static Date getDateAfterMinute(Date seedDate, int minute) {
        Calendar cal = Calendar.getInstance();
        cal.setTime(seedDate);
        cal.add(Calendar.MINUTE, minute);
        return cal.getTime();
    }


    public static String formatToHoursOnly(Date date) {
        DateFormat format = new SimpleDateFormat("hh.mm aa");
        return format.format(date).replace("AM", "am").replace("PM", "pm");
    }

    public static void main(String[] args) {
       /* List<Date> days = getHoursBetween(new Date(), new Date(new Date().getTime() + 1 * 86400 * 1000));
        System.out.println(days);*/
        System.out.println(formatToHoursOnly(new Date()));
    }
}
