package net.therap.logparser.io;

import net.therap.logparser.model.AnalysisModel;

import java.util.*;

/**
 * @author Shafin Mahmud
 * @since 11/2/16
 */
public class LogViewer {

    private final Map<Date, AnalysisModel> logAnalysis;

    public LogViewer(Map<Date, AnalysisModel> map) {
        System.out.printf("%20s%20s%30s%32s", "Time", "GET/POST Count", "Unique URI Count", "Total Response Time");
        System.out.println();

        this.logAnalysis = map;
    }

    public void printAnalysis() {
        for (Map.Entry<Date, AnalysisModel> entry : logAnalysis.entrySet()) {
            System.out.println(entry.getValue().toString());
        }
    }


    @SuppressWarnings("unchecked")
    public void printAnalysisBySortingRequestCount() {
        List<Map.Entry<Date, AnalysisModel>> entryList = new LinkedList(logAnalysis.entrySet());

        Collections.sort(entryList, new Comparator() {
            public int compare(Object o1, Object o2) {
                AnalysisModel a1 = ((Map.Entry<Date, AnalysisModel>) (o1)).getValue();
                AnalysisModel a2 = ((Map.Entry<Date, AnalysisModel>) (o2)).getValue();
                return a1.compareTo(a2);
            }
        });


        for (Map.Entry<Date, AnalysisModel> entry : entryList) {
            System.out.println(entry.getValue().toString());
        }
    }

}
