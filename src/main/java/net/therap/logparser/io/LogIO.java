package net.therap.logparser.io;


import net.therap.logparser.model.LogModel;
import net.therap.logparser.util.DateTimeUtil;
import net.therap.logparser.util.RegexUtil;

import java.io.*;
import java.util.ArrayList;
import java.util.List;


/**
 * @author Shafin Mahmud
 * @since 11/2/16
 */
public class LogIO {

    public static final String DATE_PARSING_REGEX = "[0-9]{4}-[0-9]{2}-[0-9]{2}[\\s]+[0-9]{2}:[0-9]{2}:[0-9]{2},[0-9]+";
    public static final String URI_PARSING_REGEX = "URI=\\[(.*)\\]";
    public static final String REQ_TYPE_PARSING_REGEX = "URI=\\[.*\\],[\\s]*([G|P])";
    public static final String EXECUTION_TIME_PARSING_REGEX = "(?i)time=([0-9]+)ms";

    public static List<LogModel> parseLog(String path) {
        List<LogModel> logModels  = new ArrayList<LogModel>();
        try {
            FileInputStream is = new FileInputStream(path);
            BufferedReader br = new BufferedReader(new InputStreamReader(is));

                String line;
                while ((line = br.readLine()) != null) {
                    LogModel model = parseLine(line);
                    if (!model.getUri().equals("")) {
                        logModels.add(model);
                    }
                }

        } catch (FileNotFoundException e) {
            System.out.println("Exception : File doesn't Exist!");
        } catch (IOException e) {
            e.printStackTrace();
        }
        return logModels;
    }

    private static LogModel parseLine (String line){
        LogModel model = new LogModel();

        String dateString = RegexUtil.getFirstMatchedGroup(line, DATE_PARSING_REGEX);
        model.setTime(DateTimeUtil.parseDateString(dateString));

        String uri = RegexUtil.getSecondMatchedGroup(line, URI_PARSING_REGEX);
        model.setUri(uri != null ? uri : "");

        String reqType = RegexUtil.getSecondMatchedGroup(line, REQ_TYPE_PARSING_REGEX);
        model.setRequestType(reqType != null ? reqType : "");

        String execTimeString = RegexUtil.getSecondMatchedGroup(line, EXECUTION_TIME_PARSING_REGEX);
        model.setExecutionTime(execTimeString != null ? Long.parseLong(execTimeString) : -1);

        return model;
    }

}
