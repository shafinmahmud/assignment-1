package net.therap.logparser.controller;

import net.therap.logparser.io.LogIO;
import net.therap.logparser.service.LogAnalyzer;
import net.therap.logparser.io.LogViewer;
import net.therap.logparser.model.AnalysisModel;
import net.therap.logparser.model.LogModel;

import java.util.Date;
import java.util.List;
import java.util.Map;

/**
 * @author Shafin Mahmud
 * @since 11/2/16
 */
public class LogParser {

    public static void main(String[] args) {
        if (args.length == 1) {
            String path = args[0];
            process(path, false);
        } else if (args.length == 2) {
            String path = args[0];
            boolean doSort = args[1].contains("--sort") ? true : false;
            if (doSort) {
                process(path, doSort);
            } else {
                System.out.println("Second Argument is Not Recognized. Expected --sort");
            }

        } else {
            System.out.println("Invalid Amount of Params!");
        }
    }

    public static void process(String path, boolean doSort) {
        List<LogModel> logs = LogIO.parseLog(path);

        if (logs.size() > 0) {
            LogAnalyzer analyzer = new LogAnalyzer(logs, LogAnalyzer.TimeSplit.TIME_SPLIT_BY_HOURS);
            Map<Date, AnalysisModel> map = analyzer.getTimeIntervaledAnalysis();
            LogViewer viewer = new LogViewer(map);

            if (doSort) {
                viewer.printAnalysisBySortingRequestCount();
            } else {
                viewer.printAnalysis();
            }
        }
    }
}
